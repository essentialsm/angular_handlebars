(function() {

    var CartApp = angular.module("CartApp", []);
    
    var CartConfig = function($interpolateProvider) {
        $interpolateProvider.startSymbol("{[{");
        $interpolateProvider.endSymbol("}]}");
    }
    var CartCtrl = function() {
        var vm = this;
        vm.cart = [];
        vm.item = "";
        vm.addToCard = function() {
            vm.cart.push(vm.item);
            vm.item = "";
        }
    }

    CartApp.config(["$interpolateProvider", CartConfig]);
    CartApp.controller("CartCtrl", [CartCtrl]);

})();